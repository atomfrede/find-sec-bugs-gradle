package main

import (
	"reflect"
	"strings"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/common/issue"
)

func TestConvert(t *testing.T) {
	in := `<BugCollection version="3.0.1" sequence="0" timestamp="1527870832966" analysisTimestamp="1527870833006" release="">
  <Project projectName="">
    <Jar>/tmp/project/build/classes/java/test/com/gitlab/security_products/tests/AppTest.class</Jar>
    <Jar>/tmp/project/build/classes/java/main/com/gitlab/security_products/tests/App.class</Jar>
    <SrcDir>/tmp/project/src/test/java/com/gitlab/security_products/tests/AppTest.java</SrcDir>
    <SrcDir>/tmp/project/src/main/java/com/gitlab/security_products/tests/App.java</SrcDir>
    <Plugin id="com.h3xstream.findsecbugs" enabled="true"/>
  </Project>
  <BugInstance type="CIPHER_INTEGRITY" priority="1" rank="10" abbrev="CIPINT" category="SECURITY">
    <Class classname="com.gitlab.security_products.tests.App">
      <SourceLine classname="com.gitlab.security_products.tests.App" start="14" end="48" sourcefile="App.java" sourcepath="com/gitlab/security_products/tests/App.java"/>
    </Class>
    <Method classname="com.gitlab.security_products.tests.App" name="insecureCypher" signature="()V" isStatic="false">
      <SourceLine classname="com.gitlab.security_products.tests.App" start="29" end="37" startBytecode="0" endBytecode="180" sourcefile="App.java" sourcepath="com/gitlab/security_products/tests/App.java"/>
    </Method>
    <SourceLine classname="com.gitlab.security_products.tests.App" start="29" end="29" startBytecode="2" endBytecode="2" sourcefile="App.java" sourcepath="com/gitlab/security_products/tests/App.java"/>
  </BugInstance>
  <BugInstance type="PREDICTABLE_RANDOM" priority="2" rank="12" abbrev="SECPR" category="SECURITY">
    <Class classname="com.gitlab.security_products.tests.App">
      <SourceLine classname="com.gitlab.security_products.tests.App" start="14" end="48" sourcefile="App.java" sourcepath="com/gitlab/security_products/tests/App.java"/>
    </Class>
    <Method classname="com.gitlab.security_products.tests.App" name="generateSecretToken1" signature="()Ljava/lang/String;" isStatic="false">
      <SourceLine classname="com.gitlab.security_products.tests.App" start="41" end="42" startBytecode="0" endBytecode="71" sourcefile="App.java" sourcepath="com/gitlab/security_products/tests/App.java"/>
    </Method>
    <SourceLine classname="com.gitlab.security_products.tests.App" start="41" end="41" startBytecode="4" endBytecode="4" sourcefile="App.java" sourcepath="com/gitlab/security_products/tests/App.java"/>
    <String value="java.util.Random"/>
  </BugInstance>
  <Errors errors="0" missingClasses="2">
    <MissingClass>junit.framework.TestCase</MissingClass>
    <MissingClass>junit.framework.TestSuite</MissingClass>
  </Errors>
  <FindBugsSummary timestamp="Fri, 1 Jun 2018 16:33:52 +0000" total_classes="2" referenced_classes="21" total_bugs="4" total_size="32" num_packages="1" java_version="1.8.0_171" vm_version="25.171-b11" cpu_seconds="4.34" clock_seconds="1.33" peak_mbytes="126.77" alloc_mbytes="444.50" gc_seconds="0.01" priority_2="2" priority_1="2">
    <PackageStats package="com.gitlab.security_products.tests" total_bugs="4" total_types="2" total_size="32" priority_2="2" priority_1="2">
      <ClassStats class="com.gitlab.security_products.tests.App" sourceFile="App.java" interface="false" size="23" bugs="4" priority_2="2" priority_1="2"/>
      <ClassStats class="com.gitlab.security_products.tests.AppTest" sourceFile="AppTest.java" interface="false" size="9" bugs="0"/>
    </PackageStats>
    <FindBugsProfile>
      <ClassProfile name="edu.umd.cs.findbugs.classfile.engine.ClassInfoAnalysisEngine" totalMilliseconds="173" invocations="407" avgMicrosecondsPerInvocation="426" maxMicrosecondsPerInvocation="22052" standardDeviationMircosecondsPerInvocation="1306"/>
      <ClassProfile name="edu.umd.cs.findbugs.classfile.engine.bcel.MethodGenFactory" totalMilliseconds="66" invocations="9" avgMicrosecondsPerInvocation="7413" maxMicrosecondsPerInvocation="64315" standardDeviationMircosecondsPerInvocation="20119"/>
      <ClassProfile name="edu.umd.cs.findbugs.detect.FieldItemSummary" totalMilliseconds="53" invocations="21" avgMicrosecondsPerInvocation="2553" maxMicrosecondsPerInvocation="8654" standardDeviationMircosecondsPerInvocation="2446"/>
      <ClassProfile name="edu.umd.cs.findbugs.detect.FindNoSideEffectMethods" totalMilliseconds="36" invocations="21" avgMicrosecondsPerInvocation="1723" maxMicrosecondsPerInvocation="5149" standardDeviationMircosecondsPerInvocation="1609"/>
      <ClassProfile name="edu.umd.cs.findbugs.classfile.engine.bcel.JavaClassAnalysisEngine" totalMilliseconds="34" invocations="60" avgMicrosecondsPerInvocation="566" maxMicrosecondsPerInvocation="18667" standardDeviationMircosecondsPerInvocation="2437"/>
      <ClassProfile name="edu.umd.cs.findbugs.OpcodeStack$JumpInfoFactory" totalMilliseconds="31" invocations="94" avgMicrosecondsPerInvocation="331" maxMicrosecondsPerInvocation="3669" standardDeviationMircosecondsPerInvocation="487"/>
      <ClassProfile name="edu.umd.cs.findbugs.util.TopologicalSort" totalMilliseconds="27" invocations="372" avgMicrosecondsPerInvocation="75" maxMicrosecondsPerInvocation="867" standardDeviationMircosecondsPerInvocation="121"/>
      <ClassProfile name="edu.umd.cs.findbugs.classfile.engine.ClassDataAnalysisEngine" totalMilliseconds="26" invocations="410" avgMicrosecondsPerInvocation="63" maxMicrosecondsPerInvocation="5407" standardDeviationMircosecondsPerInvocation="300"/>
      <ClassProfile name="com.h3xstream.findsecbugs.taintanalysis.TaintDataflowEngine" totalMilliseconds="22" invocations="9" avgMicrosecondsPerInvocation="2467" maxMicrosecondsPerInvocation="12236" standardDeviationMircosecondsPerInvocation="3973"/>
      <ClassProfile name="edu.umd.cs.findbugs.classfile.engine.bcel.TypeDataflowFactory" totalMilliseconds="21" invocations="9" avgMicrosecondsPerInvocation="2382" maxMicrosecondsPerInvocation="15955" standardDeviationMircosecondsPerInvocation="4871"/>
      <ClassProfile name="edu.umd.cs.findbugs.classfile.engine.bcel.IsNullValueDataflowFactory" totalMilliseconds="21" invocations="9" avgMicrosecondsPerInvocation="2348" maxMicrosecondsPerInvocation="11236" standardDeviationMircosecondsPerInvocation="3380"/>
      <ClassProfile name="edu.umd.cs.findbugs.detect.NoteDirectlyRelevantTypeQualifiers" totalMilliseconds="20" invocations="21" avgMicrosecondsPerInvocation="953" maxMicrosecondsPerInvocation="4753" standardDeviationMircosecondsPerInvocation="1225"/>
      <ClassProfile name="edu.umd.cs.findbugs.classfile.engine.bcel.CFGFactory" totalMilliseconds="19" invocations="9" avgMicrosecondsPerInvocation="2194" maxMicrosecondsPerInvocation="16360" standardDeviationMircosecondsPerInvocation="5022"/>
      <ClassProfile name="edu.umd.cs.findbugs.classfile.engine.bcel.ValueNumberDataflowFactory" totalMilliseconds="18" invocations="9" avgMicrosecondsPerInvocation="2026" maxMicrosecondsPerInvocation="14467" standardDeviationMircosecondsPerInvocation="4417"/>
      <ClassProfile name="edu.umd.cs.findbugs.detect.OverridingEqualsNotSymmetrical" totalMilliseconds="17" invocations="21" avgMicrosecondsPerInvocation="834" maxMicrosecondsPerInvocation="7642" standardDeviationMircosecondsPerInvocation="1634"/>
      <ClassProfile name="edu.umd.cs.findbugs.detect.FunctionsThatMightBeMistakenForProcedures" totalMilliseconds="17" invocations="21" avgMicrosecondsPerInvocation="829" maxMicrosecondsPerInvocation="5070" standardDeviationMircosecondsPerInvocation="1266"/>
      <ClassProfile name="edu.umd.cs.findbugs.ba.npe.NullDerefAndRedundantComparisonFinder" totalMilliseconds="14" invocations="9" avgMicrosecondsPerInvocation="1647" maxMicrosecondsPerInvocation="12583" standardDeviationMircosecondsPerInvocation="3869"/>
      <ClassProfile name="com.h3xstream.findsecbugs.PredictableRandomDetector" totalMilliseconds="13" invocations="2" avgMicrosecondsPerInvocation="6882" maxMicrosecondsPerInvocation="13672" standardDeviationMircosecondsPerInvocation="6790"/>
      <ClassProfile name="edu.umd.cs.findbugs.detect.BuildObligationPolicyDatabase" totalMilliseconds="13" invocations="21" avgMicrosecondsPerInvocation="651" maxMicrosecondsPerInvocation="4186" standardDeviationMircosecondsPerInvocation="946"/>
    </FindBugsProfile>
  </FindBugsSummary>
  <ClassFeatures></ClassFeatures>
  <History></History>
</BugCollection>`

	var scanner = issue.Scanner{
		ID:   scannerID,
		Name: scannerName,
	}

	r := strings.NewReader(in)
	want := []issue.Issue{
		{
			Category:   issue.CategorySast,
			Scanner:    scanner,
			Name:       "Cipher with no integrity",
			Message:    "Cipher with no integrity",
			CompareKey: "app/src/main/java/com/gitlab/security_products/tests/App.java:29:CIPHER_INTEGRITY",
			Severity:   issue.LevelMedium,
			Confidence: issue.LevelHigh,
			Location: issue.Location{
				File:      "app/src/main/java/com/gitlab/security_products/tests/App.java",
				LineStart: 29,
				LineEnd:   29,
				Class:     "com.gitlab.security_products.tests.App",
				Method:    "insecureCypher",
			},
			Identifiers: []issue.Identifier{
				{
					Type:  "find_sec_bugs_type",
					Name:  "Find Security Bugs-CIPHER_INTEGRITY",
					Value: "CIPHER_INTEGRITY",
					URL:   "https://find-sec-bugs.github.io/bugs.htm#CIPHER_INTEGRITY",
				},
			},
		},
		{
			Category:   issue.CategorySast,
			Scanner:    scanner,
			Name:       "Predictable pseudorandom number generator",
			Message:    "Predictable pseudorandom number generator",
			CompareKey: "app/src/main/java/com/gitlab/security_products/tests/App.java:41:PREDICTABLE_RANDOM",
			Severity:   issue.LevelMedium,
			Confidence: issue.LevelMedium,
			Location: issue.Location{
				File:      "app/src/main/java/com/gitlab/security_products/tests/App.java",
				LineStart: 41,
				LineEnd:   41,
				Class:     "com.gitlab.security_products.tests.App",
				Method:    "generateSecretToken1",
			},
			Identifiers: []issue.Identifier{
				{
					Type:  "find_sec_bugs_type",
					Name:  "Find Security Bugs-PREDICTABLE_RANDOM",
					Value: "PREDICTABLE_RANDOM",
					URL:   "https://find-sec-bugs.github.io/bugs.htm#PREDICTABLE_RANDOM",
				},
			},
		},
	}
	got, err := convert(r, "app")
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(want, got) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}
