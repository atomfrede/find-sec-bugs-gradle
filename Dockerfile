FROM gradle:jdk11-slim

COPY exclude.xml include.xml build.gradle task-only.gradle /fsb/
COPY analyzer /
ENTRYPOINT []
CMD ["/analyzer", "run"]
