package main

import (
	"encoding/xml"
	"fmt"
	"io"
	"path/filepath"
	"strconv"
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/common/issue"
)

const (
	scannerID   = "find_sec_bugs"
	scannerName = "Find Security Bugs"
)

func convert(reader io.Reader, prependPath string) ([]issue.Issue, error) {
	var doc = struct {
		BugInstances []BugInstance `xml:"BugInstance"`
	}{}

	err := xml.NewDecoder(reader).Decode(&doc)
	if err != nil {
		return nil, err
	}

	var scanner = issue.Scanner{
		ID:   scannerID,
		Name: scannerName,
	}

	issues := make([]issue.Issue, len(doc.BugInstances))
	for i, bug := range doc.BugInstances {
		r := Result{bug, prependPath}
		issues[i] = issue.Issue{
			Category:    issue.CategorySast,
			Scanner:     scanner,
			Name:        r.ShortDescription(),
			Message:     r.ShortDescription(),
			CompareKey:  r.CompareKey(),
			Severity:    r.Severity(),
			Confidence:  r.Confidence(),
			Location:    r.Location(prependPath),
			Identifiers: r.Identifiers(),
		}
	}
	return issues, nil
}

// Result describes a result with a relative path.
type Result struct {
	BugInstance
	PrependPath string
}

// Filepath returns the relative path of the affected file.
func (r Result) Filepath() string {
	// FIXME: src/main/java works only with .java files!
	return filepath.Join(r.PrependPath, "src/main/java", r.SourceLine.SourcePath)
}

// CompareKey returns a string used to establish whether two issues are the same.
func (r Result) CompareKey() string {
	start := strconv.Itoa(r.SourceLine.Start)
	return strings.Join([]string{r.Filepath(), start, r.Type}, ":")
}

// BugInstance is used to unmarshal BugInstance XML elements.
type BugInstance struct {
	Type     string `xml:"type,attr"`
	Rank     int    `xml:"rank,attr"`
	Priority int    `xml:"priority,attr"`
	Class    struct {
		Name string `xml:"classname,attr"`
	} `xml:"Class"`
	Method struct {
		Name string `xml:"name,attr"`
	} `xml:"Method"`
	SourceLine struct {
		Start      int    `xml:"start,attr"`
		End        int    `xml:"end,attr"`
		SourcePath string `xml:"sourcepath,attr"`
	} `xml:"SourceLine"`
}

// Severity returns the normalized severity of the bug instance.
// See https://github.com/spotbugs/spotbugs/blob/3.1.1/spotbugs/src/main/java/edu/umd/cs/findbugs/BugRankCategory.java#L32
func (bug BugInstance) Severity() issue.Level {
	switch bug.Rank {
	case 1, 2, 3, 4:
		return issue.LevelCritical
	case 5, 6, 7, 8, 9:
		return issue.LevelHigh
	case 10, 11, 12, 13, 14:
		return issue.LevelMedium
	case 15, 16, 17, 18, 19, 20:
		return issue.LevelLow
	}
	return issue.LevelUnknown
}

// Confidence returns the normalized confidence of the bug instance.
// See https://github.com/spotbugs/spotbugs/blob/3.1.1/spotbugs/src/main/java/edu/umd/cs/findbugs/Priorities.java
func (bug BugInstance) Confidence() issue.Level {
	switch bug.Priority {
	case 1:
		return issue.LevelHigh
	case 2:
		return issue.LevelMedium
	case 3:
		return issue.LevelLow
	case 4:
		return issue.LevelExperimental
	case 5:
		return issue.LevelIgnore
	}
	return issue.LevelUnknown
}

// Location returns a structured Location
func (bug BugInstance) Location(prependPath string) issue.Location {
	return issue.Location{
		File:      filepath.Join(prependPath, "src/main/java", bug.SourceLine.SourcePath),
		LineStart: bug.SourceLine.Start,
		LineEnd:   bug.SourceLine.End,
		Class:     bug.Class.Name,
		Method:    bug.Method.Name,
	}
}

// Identifiers returns the normalized identifiers of the vulnerability.
func (bug BugInstance) Identifiers() []issue.Identifier {
	return []issue.Identifier{
		bug.FSBIdentifier(),
	}
}

// FSBIdentifier returns a structured Identifier for a FSB bug Type
func (bug BugInstance) FSBIdentifier() issue.Identifier {
	return issue.Identifier{
		Type:  "find_sec_bugs_type",
		Name:  fmt.Sprintf("Find Security Bugs-%s", bug.Type),
		Value: bug.Type,
		URL:   fmt.Sprintf("https://find-sec-bugs.github.io/bugs.htm#%s", bug.Type),
	}
}

// ShortDescription returns a description for the given vulnerability type.
func (bug BugInstance) ShortDescription() string {
	return messages[bug.Type]
}

// Messages is a map based on Find Sec Bugs' message.xml.
// See https://raw.githubusercontent.com/find-sec-bugs/find-sec-bugs/master/plugin/src/main/resources/metadata/messages.xml
var messages = map[string]string{
	"PREDICTABLE_RANDOM":                            "Predictable pseudorandom number generator",
	"PREDICTABLE_RANDOM_SCALA":                      "Predictable pseudorandom number generator (Scala)",
	"SERVLET_PARAMETER":                             "Untrusted servlet parameter",
	"SERVLET_CONTENT_TYPE":                          "Untrusted Content-Type header",
	"SERVLET_SERVER_NAME":                           "Untrusted Hostname header",
	"SERVLET_SESSION_ID":                            "Untrusted session cookie value",
	"SERVLET_QUERY_STRING":                          "Untrusted query string",
	"SERVLET_HEADER":                                "HTTP headers untrusted",
	"SERVLET_HEADER_REFERER":                        "Untrusted Referer header",
	"SERVLET_HEADER_USER_AGENT":                     "Untrusted User-Agent header",
	"COOKIE_USAGE":                                  "Potentially sensitive data in a cookie",
	"PATH_TRAVERSAL_IN":                             "Potential Path Traversal (file read)",
	"PATH_TRAVERSAL_OUT":                            "Potential Path Traversal (file write)",
	"SCALA_PATH_TRAVERSAL_IN":                       "Potential Path Traversal (file read)",
	"COMMAND_INJECTION":                             "Potential Command Injection",
	"SCALA_COMMAND_INJECTION":                       "Potential Command Injection (Scala)",
	"WEAK_FILENAMEUTILS":                            "FilenameUtils not filtering null bytes",
	"WEAK_TRUST_MANAGER":                            "TrustManager that accept any certificates",
	"WEAK_HOSTNAME_VERIFIER":                        "HostnameVerifier that accept any signed certificates",
	"JAXWS_ENDPOINT":                                "Found JAX-WS SOAP endpoint",
	"JAXRS_ENDPOINT":                                "Found JAX-RS REST endpoint",
	"TAPESTRY_ENDPOINT":                             "Found Tapestry page",
	"WICKET_ENDPOINT":                               "Found Wicket WebPage",
	"WEAK_MESSAGE_DIGEST_MD5":                       "MD2, MD4 and MD5 are weak hash functions",
	"WEAK_MESSAGE_DIGEST_SHA1":                      "SHA-1 is a weak hash function",
	"DEFAULT_HTTP_CLIENT":                           "DefaultHttpClient with default constructor is not compatible with TLS 1.2",
	"SSL_CONTEXT":                                   "Weak SSLContext",
	"CUSTOM_MESSAGE_DIGEST":                         "Message digest is custom",
	"FILE_UPLOAD_FILENAME":                          "Tainted filename read",
	"REDOS":                                         "Regex DOS (ReDOS)",
	"XXE_XMLSTREAMREADER":                           "XML parsing vulnerable to XXE (XMLStreamReader)",
	"XXE_SAXPARSER":                                 "XML parsing vulnerable to XXE (SAXParser)",
	"XXE_XMLREADER":                                 "XML parsing vulnerable to XXE (XMLReader)",
	"XXE_DOCUMENT":                                  "XML parsing vulnerable to XXE (DocumentBuilder)",
	"XXE_DTD_TRANSFORM_FACTORY":                     "XML parsing vulnerable to XXE (TransformerFactory)",
	"XXE_XSLT_TRANSFORM_FACTORY":                    "XSLT parsing vulnerable to XXE (TransformerFactory)",
	"XPATH_INJECTION":                               "Potential XPath Injection",
	"STRUTS1_ENDPOINT":                              "Found Struts 1 endpoint",
	"STRUTS2_ENDPOINT":                              "Found Struts 2 endpoint",
	"SPRING_ENDPOINT":                               "Found Spring endpoint",
	"SPRING_CSRF_PROTECTION_DISABLED":               "Spring CSRF protection disabled",
	"SPRING_CSRF_UNRESTRICTED_REQUEST_MAPPING":      "Spring CSRF unrestricted RequestMapping",
	"CUSTOM_INJECTION":                              "Potential injection (custom)",
	"SQL_INJECTION":                                 "Potential SQL Injection",
	"SQL_INJECTION_TURBINE":                         "Potential SQL Injection with Turbine",
	"SQL_INJECTION_HIBERNATE":                       "Potential SQL/HQL Injection (Hibernate)",
	"SQL_INJECTION_JDO":                             "Potential SQL/JDOQL Injection (JDO)",
	"SQL_INJECTION_JPA":                             "Potential SQL/JPQL Injection (JPA)",
	"SQL_INJECTION_SPRING_JDBC":                     "Potential JDBC Injection (Spring JDBC)",
	"SQL_INJECTION_JDBC":                            "Potential JDBC Injection",
	"SCALA_SQL_INJECTION_SLICK":                     "Potential Scala Slick Injection",
	"SCALA_SQL_INJECTION_ANORM":                     "Potential Scala Anorm Injection",
	"SQL_INJECTION_ANDROID":                         "Potential Android SQL Injection",
	"LDAP_INJECTION":                                "Potential LDAP Injection",
	"SCRIPT_ENGINE_INJECTION":                       "Potential code injection when using Script Engine",
	"SPEL_INJECTION":                                "Potential code injection when using Spring Expression",
	"EL_INJECTION":                                  "Potential code injection when using Expression Language (EL)",
	"SEAM_LOG_INJECTION":                            "Potential code injection in Seam logging call",
	"OGNL_INJECTION":                                "Potential code injection when using OGNL expression",
	"HTTP_RESPONSE_SPLITTING":                       "Potential HTTP Response Splitting",
	"CRLF_INJECTION_LOGS":                           "Potential CRLF Injection for logs",
	"EXTERNAL_CONFIG_CONTROL":                       "Potential external control of configuration",
	"BAD_HEXA_CONVERSION":                           "Bad hexadecimal concatenation",
	"HAZELCAST_SYMMETRIC_ENCRYPTION":                "Hazelcast symmetric encryption",
	"NULL_CIPHER":                                   "NullCipher is insecure",
	"UNENCRYPTED_SOCKET":                            "Unencrypted Socket",
	"UNENCRYPTED_SERVER_SOCKET":                     "Unencrypted Server Socket",
	"DES_USAGE":                                     "DES is insecure",
	"TDES_USAGE":                                    "DESede is insecure",
	"RSA_NO_PADDING":                                "RSA with no padding is insecure",
	"HARD_CODE_PASSWORD":                            "Hard Coded Password",
	"HARD_CODE_KEY":                                 "Hard Coded Key",
	"UNSAFE_HASH_EQUALS":                            "Unsafe hash equals",
	"STRUTS_FORM_VALIDATION":                        "Struts Form without input validation",
	"XSS_REQUEST_WRAPPER":                           "XSSRequestWrapper is a weak XSS protection",
	"BLOWFISH_KEY_SIZE":                             "Blowfish usage with short key",
	"RSA_KEY_SIZE":                                  "RSA usage with short key",
	"UNVALIDATED_REDIRECT":                          "Unvalidated Redirect",
	"PLAY_UNVALIDATED_REDIRECT":                     "Unvalidated Redirect (Play Framework)",
	"SPRING_UNVALIDATED_REDIRECT":                   "Spring Unvalidated Redirect",
	"JSP_INCLUDE":                                   "Dynamic JSP inclusion",
	"JSP_SPRING_EVAL":                               "Dynamic variable in Spring expression",
	"JSP_JSTL_OUT":                                  "Escaping of special XML characters is disabled",
	"XSS_JSP_PRINT":                                 "Potential XSS in JSP",
	"XSS_SERVLET":                                   "Potential XSS in Servlet",
	"XML_DECODER":                                   "XMLDecoder usage",
	"STATIC_IV":                                     "Static IV",
	"ECB_MODE":                                      "ECB mode is insecure",
	"PADDING_ORACLE":                                "Cipher is susceptible to Padding Oracle",
	"CIPHER_INTEGRITY":                              "Cipher with no integrity",
	"ESAPI_ENCRYPTOR":                               "Use of ESAPI Encryptor",
	"ANDROID_EXTERNAL_FILE_ACCESS":                  "External file access (Android)",
	"ANDROID_BROADCAST":                             "Broadcast (Android)",
	"ANDROID_WORLD_WRITABLE":                        "World writable file (Android)",
	"ANDROID_GEOLOCATION":                           "WebView with geolocation activated (Android)",
	"ANDROID_WEB_VIEW_JAVASCRIPT":                   "WebView with JavaScript enabled (Android)",
	"ANDROID_WEB_VIEW_JAVASCRIPT_INTERFACE":         "WebView with JavaScript interface (Android)",
	"INSECURE_COOKIE":                               "Cookie without the secure flag",
	"HTTPONLY_COOKIE":                               "Cookie without the HttpOnly flag",
	"OBJECT_DESERIALIZATION":                        "Object deserialization is used in {1}",
	"JACKSON_UNSAFE_DESERIALIZATION":                "Unsafe Jackson deserialization configuration",
	"DESERIALIZATION_GADGET":                        "This class could be used as deserialization gadget",
	"TRUST_BOUNDARY_VIOLATION":                      "Trust Boundary Violation",
	"JSP_XSLT":                                      "A malicious XSLT could be provided",
	"MALICIOUS_XSLT":                                "A malicious XSLT could be provided",
	"SCALA_SENSITIVE_DATA_EXPOSURE":                 "Potential information leakage in Scala Play",
	"SCALA_PLAY_SSRF":                               "Scala Play Server-Side Request Forgery (SSRF)",
	"URLCONNECTION_SSRF_FD":                         "URLConnection Server-Side Request Forgery (SSRF) and File Disclosure",
	"SCALA_XSS_TWIRL":                               "Potential XSS in Scala Twirl template engine",
	"SCALA_XSS_MVC_API":                             "Potential XSS in Scala MVC API engine",
	"TEMPLATE_INJECTION_VELOCITY":                   "Potential template injection with Velocity",
	"TEMPLATE_INJECTION_FREEMARKER":                 "Potential template injection with Freemarker",
	"PERMISSIVE_CORS":                               "Overly permissive CORS policy",
	"LDAP_ANONYMOUS":                                "Anonymous LDAP bind",
	"LDAP_ENTRY_POISONING":                          "LDAP Entry Poisoning",
	"COOKIE_PERSISTENT":                             "Persistent Cookie Usage",
	"URL_REWRITING":                                 "URL rewriting method",
	"INSECURE_SMTP_SSL":                             "Insecure SMTP SSL connection",
	"AWS_QUERY_INJECTION":                           "AWS Query Injection",
	"BEAN_PROPERTY_INJECTION":                       "JavaBeans Property Injection",
	"STRUTS_FILE_DISCLOSURE":                        "Struts File Disclosure",
	"SPRING_FILE_DISCLOSURE":                        "Spring File Disclosure",
	"REQUESTDISPATCHER_FILE_DISCLOSURE":             "RequestDispatcher File Disclosure",
	"FORMAT_STRING_MANIPULATION":                    "Format String Manipulation",
	"HTTP_PARAMETER_POLLUTION":                      "HTTP Parameter Pollution",
	"INFORMATION_EXPOSURE_THROUGH_AN_ERROR_MESSAGE": "Information Exposure Through An Error Message",
	"SMTP_HEADER_INJECTION":                         "SMTP Header Injection",
}
