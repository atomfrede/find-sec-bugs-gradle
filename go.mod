module gitlab.com/gitlab-org/security-products/analyzers/find-sec-bugs-gradle

require (
	github.com/urfave/cli v1.20.0
	gitlab.com/gitlab-org/security-products/analyzers/common v1.6.0
)
